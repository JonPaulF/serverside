<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Questionnaire;

class Question extends Model
{

  protected $fillable = ['title', 'Answer1', 'Answer2', 'Answer3','Answer4','Answer5', 'user_id'];


  public function questionnaires() {
    return $this->belongsTo(Questionnaire::class);
  }

  public function user() {
    return $this->belongsTo(User::class);
  }

  public function answers() {
    return $this->hasMany(Answer::class);
  }

  protected $table = 'question';

}
