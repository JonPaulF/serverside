<?php

Route::get('/', 'QuestionnaireController@home');
//this is the route for the homepage

Route::get('/Questionnaires', 'QuestionnaireController@index');
//this is the route which shows all of the different questionnaires

Route::group(['middleware' => ['web']], function () {
    //
    Route::auth();

    Route::get('/Questionnaire/new', 'QuestionnaireController@new_Questionnaire')->name('new.Questionnaire'); //YES
    //This is the route which enables the user to create a new questionnaire
    Route::get('/Questionnaire/{Questionnaire}', 'QuestionnaireController@detail_Questionnaire')->name('detail.Questionnaire');
    //This is the route for the page where the questionnaire is being created so questions can be added
    Route::get('/Questionnaire/view/{Questionnaire}', 'QuestionnaireController@view')->name('view.Questionnaire');
    //This is the view for all of the individual questionnaires
    Route::get('/answers', 'QuestionnaireController@QuestionAnswers');
    //This is the answers page which shows the list of questionnaires that have been responded too
    Route::get('/answers/{questionnaire}', 'QuestionnaireController@view_Questionnaire_answers');
    //This shows the responses for specific questionnaires, dependant on which has been clicked on
    Route::get('/Questionnaire/{Questionnaire}/edit', 'QuestionnaireController@edit')->name('edit.Questionnaire');
    //This allows the user to edit the questionnaire title and description
    Route::patch('/Questionnaire/{Questionnaire}/update', 'QuestionnaireController@update')->name('update.Questionnaire');
    //This allows the user to update the questionnaire
    Route::post('/Questionnaire/view/{Questionnaire}/completed', 'AnswerController@store')->name('complete.Questionnaire');
    //This gives a thank-you message for the user from the store method in the answer controller
    Route::post('/Questionnaire/create', 'QuestionnaireController@create')->name('create.Questionnaire');

    // Questions related
    Route::post('/Questionnaire/{Questionnaire}/questions', 'QuestionController@store')->name('store.question');

    Route::get('/question/{question}/edit', 'QuestionController@edit')->name('edit.question');
    Route::patch('/question/{question}/update', 'QuestionController@update')->name('update.question');

    Route::get('/home', 'HomeController@index');



});
