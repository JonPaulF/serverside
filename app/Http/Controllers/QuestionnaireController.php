<?php

namespace App\Http\Controllers;

use Auth;
use App\questionnaire;
use App\Answer;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Question;
use App\Http\Controllers\DB;


class QuestionnaireController extends Controller
{

  public function __construct()
  {
 $this->middleware('auth', ['except' => ['index' ,'view']]);

  }
  public function QuestionAnswers()
  {
    $Questionnaire = questionnaire::all();


    return view('questionnaire/ViewAllQuestions', ['questionnaires' => $Questionnaire]);
  }

  public function index()
  {
    $Questionnaire = questionnaire::all();


    return view('questionnaire/ViewAll', ['questionnaires' => $Questionnaire]);
  }


    //$Questionnaire->get('user.questions.answers');
  public function home(Request $request)
  {
    $Questionnaires = Questionnaire::get();
    return view('home', compact('Questionnaires'));
  }

  # Show page to create new Questionnaire
  public function new_Questionnaire()
  {
    return view('questionnaire.new');
  }

  public function create(Request $request, Questionnaire $Questionnaire)
  {

    $arr = $request->all();
    // $request->all()['user_id'] = Auth::id();
    $arr['user_id'] = Auth::id();
    $QuestionnaireItem = $Questionnaire->create($arr);
    return Redirect::to("/Questionnaire/{$QuestionnaireItem->id}");
  }

  # retrieve detail page and add questions here
  public function detail_Questionnaire(Questionnaire $Questionnaire)
  {
    $Questionnaire->load('questions.user');
    return view('questionnaire.detail', compact('Questionnaire'));
  }


  public function edit(Questionnaire $Questionnaire)
  {
    return view('questionnaire.edit', compact('Questionnaire'));
  }

  # edit Questionnaire
  public function update(Request $request, Questionnaire $Questionnaire)
  {
    $Questionnaire->update($request->only(['title', 'description']));
    return redirect()->action('QuestionnaireController@detail_Questionnaire', [$Questionnaire->id]);
  }

  # view Questionnaire publicly and complete Questionnaire



  public function view($id)
  {
    $questionnaire = Questionnaire::findOrFail($id);


    $questionnaire->load('questions.user');
    foreach ($questionnaire->questions as $question) {
      $answers = array();
      if (strlen($question->Answer1) > 0){
        $answers[$question->Answer1] = $question->Answer1;
      }
      if (strlen($question->Answer2) > 0){
        $answers[$question->Answer2] = $question->Answer2;
      }
      if (strlen($question->Answer3) > 0){
        $answers[$question->Answer3] = $question->Answer3;
      }
      if (strlen($question->Answer4) > 0){
        $answers[$question->Answer4] = $question->Answer4;
      }
      if (strlen($question->Answer5) > 0){
        $answers[$question->Answer5] = $question->Answer5;
      }
      $question['answers'] = $answers;
    }
    return view('questionnaire.view', compact('questionnaire', $questionnaire));


    $Questionnaire->get('user.questions.answers');
  }

  # view submitted answers from current logged in user
  public function view_Questionnaire_answers($questionnaire)
  {

    //$Questionnaire->get('user.questions.answers');
    $questionnaire_fetched = questionnaire::findOrFail($questionnaire);
    $questions = Question::where('questionnaire_id', $questionnaire_fetched->id)->get();


    foreach($questions as $question){
      $responses = array();
      $question_id = $question->id;
      //answers 1
      $answer1 = Answer::where('answer', $question->Answer1)->count();
      array_push($responses, $answer1);
      $answer2 = Answer::where('answer', $question->Answer2)->count();
      array_push($responses, $answer2);
      $answer3 = Answer::where('answer', $question->Answer3)->count();
      array_push($responses, $answer3);
      $answer4 = Answer::where('answer', $question->Answer4)->count();
      array_push($responses, $answer4);
      $answer5 = Answer::where('answer', $question->Answer5)->count();
      array_push($responses, $answer5);
      $question['response'] = $responses;
    }
    $questionnaire_fetched['questions'] = $questions;
//   return $questionnaire_fetched;
    return view('answer.view', compact('questionnaire_fetched'));
  }


  public function delete_Questionnaire(Questionnaire $Questionnaire)
  {
    $Questionnaire->delete();
    return redirect('');
  }

}
