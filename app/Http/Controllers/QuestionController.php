<?php

namespace App\Http\Controllers;

use App\Questionnaire;
use App\Question;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use App\Http\Requests;

class QuestionController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth' ,['except' => ['store']]);
  }


    public function view_Question(Question $Question)
    {

      $cats = question::where('question',$Question)->pluck('');

      return view('question.view', compact('question', 'cats'));

    }

    public function store(Request $request, Questionnaire $Questionnaire)
    {
      $arr = $request->all();
      $arr['user_id'] = Auth::id();


      $Questionnaire->questions()->create($arr);
      return back();
    }

    public function edit(Question $question)
    {
      return view('question.edit', compact('question'));
      $cats = array($Question->id, $Questionnaire->Answer2);

      return view('question.edit', compact('question', 'cats'));
    }

    public function update(Request $request, Question $question)
    {

      $question->update($request->all());
      return redirect()->action('QuestionnaireController@detail_Questionnaire', [$question->Questionnaire_id]);
    }

}
