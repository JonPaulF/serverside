<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Questionnaire;
use App\Answer;
use App\Http\Requests;


class AnswerController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth', ['except' => ['store']]);
  }

  public function store(Request $request)
  {
    $answers = array();
    $input = $request->all();

    //array_shift($input);

    // foreach ($input as $key=>$answer) {
    // //  return key($input);
    //   array_push($answers, $key);
    // }
    //dd($request->all());
    $keys = array_keys($request->all());
    //return var_dump($keys);


    //return $keys;
    for($i = 0; $i < count($input); $i++){
    if($keys[$i] !== '_token'){
      if($keys[$i] !== 'questionnaire_id'){
        $question = $keys[$i];
        $answer = $input[$keys[$i]];
        $save = [
        'questionnaire_id' => $input['questionnaire_id'],
        'question_id' => $question,
        'answer' => $answer
          ];
        //  return $save;
          $saved = Answer::create($save);
        }
      }
    }

    //eturn var_dump($answers);
   return view('/questionnaire/completed');
  }
}
