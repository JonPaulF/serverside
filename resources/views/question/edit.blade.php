

<form method="POST" action="/question/{{ $question->id }}/update">
  {{ method_field('PATCH') }}
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <h2 class="flow-text">Edit Question Title</h2>
   <div class="row">
    <div class="input-field col s12">
      <input type="text" name="title" id="title" value="{{ $question->title }}">
      <label for="title">Question</label>
    </div>

    <div class="input-field col s12">
    <input type="text" name="Answer1" id="Answer1" value="{{ $question->Answer1 }}">
    <label for="title">Answer1</label>
    <div>

    <div class="input-field col s12">
    <input type="text" name="Answer2" id="Answer2" value="{{ $question->Answer2 }}">
    <label for="title">Answer2</label>
    <div>


    <div class="input-field col s12">
    <button class="btn waves-effect waves-light">Update</button>
    </div>
  </div>
</form>
