@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $questionnaire_fetched->title }}</div>
  <table>
    <style type="text/css">
      td
        {
          padding:15px;
        }
      th
      {
        padding:15px;
      }

    </style>
    <thead>
      <tr>
        <th >Question</th>
        <th>Answer 1</th>
        <th>Answer 2</th>
        <th>Answer 3</th>
        <th>Answer 4 </th>
        <th>Answer 5</th>
      </tr>
    </thead>

  <tbody>
    @foreach ($questionnaire_fetched->questions as $question)
      <tr>

        <td>{{$question->title}} </td>
        <td>{{$question->Answer1}} - {{$question['response'][0]}} </td>
        <td>{{$question->Answer2}} - {{$question['response'][1]}} </td>
        <td>{{$question->Answer3}} - {{$question['response'][2]}} </td>
        <td>{{$question->Answer4}} - {{$question['response'][3]}} </td>
        <td>{{$question->Answer5}} - {{$question['response'][4]}} </td>

      </tr>
    @endforeach
  </tbody>


</table>
<p> Provided above is the amount of times an answer has been made, The Question title for the questionnaire and then also <br>
    a number next to this which is the amount of times that the specific answer has been chosen.
</p>
@endsection
