@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Questionnaire responses</div>

                <section>
                    @if (isset ($questionnaires))
                    <ul>
                    @foreach ($questionnaires as $questionnaire)
                        <li><a href="answers/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
                    @endforeach
                    </ul>
                    @else
                        <p> No Questionnaires added yet </p>
                    @endif
                </section>

            <p>To see a questionnaire response please click on the questionnaire you would like to view by using the list provided above</p
    </div>
  </div>
  @endsection
