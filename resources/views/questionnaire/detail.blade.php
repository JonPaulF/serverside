@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $Questionnaire->title }}</div>
      <div class="panel-body">
      <p>
        <p>Questionnaire Description </p>{{ $Questionnaire->description }}
      </p>
      <br/>
      <a href='view/{{$Questionnaire->id}}'>Take Questionnaire</a> | <a href="{{$Questionnaire->id}}/edit">Edit Questionnaire</a> | <a href="/Questionnaire/answers/{{$Questionnaire->id}}">View Answers</a> <a href="#doDelete" style="float:right;" class="modal-trigger red-text">Delete Questionnaire</a>
      <!-- Modal Structure -->
      <!-- TODO Fix the Delete aspect -->
      <div id="doDelete" class="modal bottom-sheet">
        <div class="modal-content">
          <div class="container">
            <div class="row">
              <h4>Are you sure?</h4>
              <p>Do you wish to delete this Questionnaire called "{{ $Questionnaire->title }}"?</p>
              <div class="modal-footer">
                <a href="/Questionnaire/{{ $Questionnaire->id }}/delete" class=" modal-action waves-effect waves-light btn-flat red-text">Yep yep!</a>
                <a class=" modal-action modal-close waves-effect waves-light green white-text btn">No, stop!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="divider" style="margin:20px 0px;"></div>
      <p class="flow-text center-align">Questions</p>
      <ul class="collapsible" data-collapsible="expandable">
          @forelse ($Questionnaire->questions as $question)
          <li style="box-shadow:none;">
            <div class="collapsible-header">{{ $question->title }} <a href="/question/{{ $question->id }}/edit" style="float:right;">Edit</a></div>
            <div class="collapsible-body">
              <div style="margin:5px; padding:10px;">

              </div>
            </div>
          </li>
          @empty
            <span style="padding:10px;">Nothing to show. Add questions below.</span>
          @endforelse
      </ul>
      <h2 class="flow-text">Add Question</h2>
      <form method="POST" action="{{ $Questionnaire->id }}/questions" id="boolean">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">

          <div class="input-field col s12">
            <input name="title" id="title" type="text">
            <label for="title">Question</label>
          </div>
          <div class="input-field col s12">
            <input name="Answer1" id="Answer1" type="text">
            <label for="title">Answer 1</label>
          </div>
          <div class="input-field col s12">
            <input name="Answer2" id="Answer2" type="text">
            <label for="title">Answer 2</label>
          </div>
          <div class="input-field col s12">
            <input name="Answer3" id="Answer3" type="text">
            <label for="title">Answer 3</label>
          </div>
          <div class="input-field col s12">
            <input name="Answer4" id="Answer4" type="text">
            <label for="title">Answer 4</label>
          </div>
          <div class="input-field col s12">
            <input name="Answer5" id="Answer5" type="text">
            <label for="title">Answer 5</label>
          </div>
          <!-- this part will be chewed by script in init.js -->
          <span class="form-g"></span>

          <div class="input-field col s12">
          <button class="btn btn-large btn-primary openbutton">Submit</button>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>
  @endsection
