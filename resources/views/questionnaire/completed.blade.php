@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                  <div class="panel-heading">Questionnaire completed</div>
                  <br>
                  <p> Thankyou for completing the questionnaire, we value your responses</p>
                  <br>
                  <p> Please use the navigation bar to navigate through the website
        <div class="panel-body">
    </div>
  </div>
</div>
@endsection
