@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Questionnaire</div>
<div class="panel-body">
      <form method="POST" action="create" id="boolean">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div>
            <input name="title" id="title" type="text"placeholder="Enter Title" >
            <label for="title">Questionnaire Title</label>
          </div>
          <div>
            <textarea rows=5 cols=28 name="description" id="description" placeholder="Enter Description"  ></textarea>
            <label for="description">Description</label>
          </div>
          <div class="input-field col s12" >
          <button class ='btn btn-large btn-primary openbutton'>Submit</button>
          </div>
        </div>
        </form>
    </div>
  </div>
</div>
@endsection
