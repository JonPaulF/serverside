@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"> Start taking questionnaire</div>

        <div class="panel-body">
        <span class="flow-text">{{ $questionnaire->title }}</span> <br/>
      </p>
      <p>
        {{ $questionnaire->description }}
        <br/>Created by: <a href="">{{ $questionnaire->user->name }}</a>
      </p>
      <div class="divider" style="margin:20px 0px;"></div>

          {!! Form::open(array('action'=>array('AnswerController@store', $questionnaire->id))) !!}
            {!! Form::hidden('questionnaire_id', $questionnaire->id) !!}
          @foreach ($questionnaire->questions as $question)
            <p class="flow-text"> {{ $question->title }}</p>

            <div class>
              {!! Form::select( $question->id, $question['answers'], null,['class' => 'large-8 columns']) !!}
            </div>

            @endforeach

          <br>

          <div class>
        {!! Form::submit('Submit questionnaire', ['class' => 'button']) !!}
          </div>
          {!! Form::close() !!}
        </div>
    </div>
  </div>
  @endsection
