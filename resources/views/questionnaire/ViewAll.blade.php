@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Questionnaires</div>

                <section>
                    @if (isset ($questionnaires))
                    <ul>
                    @foreach ($questionnaires as $questionnaire)
                        <li><a href="Questionnaire/view/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
                    @endforeach
                    </ul>
                    @else
                        <p> No Questionnaires added yet </p>
                    @endif
                </section>
    </div>
  </div>
  @endsection
