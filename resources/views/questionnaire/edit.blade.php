@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add Questionnaire</div>
                  <div class="panel-body">
                    <form method="POST" action="/Questionnaire/{{ $Questionnaire->id }}/update">
                      <!--This is responsible for updating the current questionnaire by using the ID-->
                      {{ method_field('PATCH') }}
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <h2 class="flow-text">Edit Questionnaire</h2>
                      <div class="row">
                        <div class="input-field col s12">
                          <input type="text" name="title" id="title" value="{{ $Questionnaire->title }}">
                          <!--This gets the questionnaie title-->
                          <label for="title">Question</label>
                        </div>
                        <div class="input-field col s12">
                          <textarea id="description" name="description">{{ $Questionnaire->description }}</textarea>
                          <!--This gets the questionnaire description-->
                          <label for="description">Description</label>
                        </div>
                      <div class="input-field col s12">
                <button class="btn waves-effect waves-light">Update</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
