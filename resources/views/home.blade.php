@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                </p>
                    <p> Welcome the the questionnaire site, if you would like to participate <Br>
                      in creating a questionnaire then please use the above links to navigate <br>
                      around the site.These links will allow you to  locate all the <br>
                      questionnaires in which you are able to answer.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
