<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
          //this creates the table 'question'
            $table->increments('id');
            //sets the id of the question
            $table->integer('questionnaire_id')->unsigned()->index();
            //adds the questionnaire_id the question was created on
            $table->integer('user_id')->unsigned()->index();
            //adds the user id of the person who created the question
            $table->string('title');
            //adds the question title
            $table->string('Answer1');
            //gives answer option 1
            $table->string('Answer2');
            //gives answer option 2
            $table->string('Answer3');
            //gives answer option 3
            $table->string('Answer4');
            //gives answer option 4
            $table->string('Answer5');
            //gives answer option 5
            $table->timestamps();
            //creates a  timestamp for the question 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question');
    }
}
