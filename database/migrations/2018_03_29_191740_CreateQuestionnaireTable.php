<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaire', function (Blueprint $table) {#
          //This creates a table 'questionnaire' and sets the name to 'questionnaire'
            $table->increments('id');
            //this is the id of the questionnaire
            $table->string('title');
            //this is where the title of the questionnaires will be stored
            $table->integer('user_id')->unsigned()->index();
            //this is the user id which will be used later to show who has created the questionnaire
            $table->string('description');
            //this is the description of the questionnaire to explain what it is about. the user will be the person who adds this
            $table->timestamps();
            //this will store the time that the questionnaire was created.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questionnaire');
    }
}
