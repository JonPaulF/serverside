<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Answer', function (Blueprint $table) {
          //this creates the table 'Answer'
            $table->increments('id');
            //this is the answers id
            $table->integer('user_id');
            //this will store the users id
            $table->integer('question_id');
            //this adds the question id
            $table->integer('questionnaire_id');
            //this is the questionnaire id which is being answered
            $table->string('answer');
            //this is where the final answer out of the five will be stored
            $table->timestamps();
            //a single timestamp of the answer and when it was answered
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Answer');
    }
}
