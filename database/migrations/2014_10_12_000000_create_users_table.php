<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          //This creates the table and sets the schemas name as 'users'
            $table->increments('id');
            //this is the 'id' of the account
            $table->string('name');
            //this allows the name of the user to be stored
            $table->string('email')->unique();
            //this allows the e-mail of the user to be stored and makes sure its unique
            $table->string('password');
            //this is where the password of the account will be stored
            $table->rememberToken();
            $table->timestamps();
        });
        //The create users table is part of the authentication and allows accounts to be created ready for logging in
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
