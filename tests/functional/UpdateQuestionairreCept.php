<?php
 $I = new FunctionalTester($scenario);

 $I->am('user');
 $I->wantTo('update an questionairre');

 // Add db test data

 // add a test user
 $I->haveRecord('users', [
     'id' => '9999',
     'name' => 'testuser1',
     'email' => 'test1@user.com',
     'password' => 'password',
 ]);

 // Add test categories
 $I->haveRecord('categories', [
     'id' => '9900',
     'title' => 'category1',
     'detail' => 'category1 detail',
 ]);
 $I->haveRecord('categories', [
     'id' => '9901',
     'title' => 'category2',
     'detail' => 'category2 detail',
 ]);


 // add a test questionairre to check that content can be seen in list at start

 $I->haveRecord('questionairres', [
     'id' => '9000',
     'title' => 'questionairre 1',
     'content' => 'questionairre 1 content',
     'slug' => 'questionairre1',
     'user_id' => 'testuser1',
 ]);


 // add link data for questionairre and category for the test entry
 $I->haveRecord('questionairre_category', [
     'questionairre_id' => '9090',
     'category_id' => '9900',
 ]);




 // tests /////////////////////////////////////////////

 // create an questionairre linked to one category
 // When
 $I->amOnPage('/admin/questionairres');
 $I->see('questionairres', 'h1');
 $I->see('questionairre 1');


 // Then

 // Check  the link is present - this is because there could potentially be many update links/buttons.
 // each link can be identified by the users id as name.
 $I->seeElement('a', ['name' => '9000']);
 // And
 $I->click('a', ['name' => '9000']);

 // Then
 $I->amOnPage('/admin/questionairres/9000/edit');
 // And
 $I->see('Edit questionairre - questionairre1', 'h1');

 // Then
 $I->fillField('title', 'Updatedtitle');
 // And
 $I->click('Update questionairre');

 // Then
 $I->seeCurrentUrlEquals('admin/questionairres');
 $I->seeRecord('questionairres', ['title' => 'Updatedtitile']);
 $I->see('questionairres', 'h1');
 $I->see('Updatedtitile');
