<?php
  $I = new FunctionalTester($scenario);

  $I->am('admin');
  $I->wantTo('create a new questionnaire');

  Auth::loginUsingId(1);
  // Add db test data

  // add a test user
  $I->haveRecord('users', [
      'id' => '9999',
      'name' => 'testuser1',
      'email' => 'test1@user.com',
      'password' => 'password',
  ]);

  // Add test questionnaire
  $I->haveRecord('questionnaires', [
      'id' => '9900',
      'title' => 'questionnaire 1',
      'description' => 'questionnaire 1 description',
  ]);
  $I->haveRecord('questionnaires', [
      'id' => '9901',
      'title' => 'questionnaire2',
      'description' => 'questionnaire 2 description',
  ]);


  // add a test questionnaire to check that content can be seen in list at start

  $I->haveRecord('question', [
      'id' => '9900',
      'question' => 'question1',
      'detail' => 'question 1 detail',
  ]);


  // add link data for questionnaire and questionnaire for the test entry





  // tests /////////////////////////////////////////////

  // create an questionnaire linked to one questionnaire
  // When
  $I->amOnPage('/admin/questionnaires');
  $I->see('Questionnaires');
  $I->see('Questionnaire 1');
  $I->dontSee('Questionnaire 2');
  // And
  $I->click('Add Questionnaire');

  // Then
  $I->amOnPage('/admin/questionnaires/create');
  // And
  $I->see('Add Questionnaire');

  $I->submitForm('#createquestionnaire', [
      'title' => 'Questionnaire 2',
      'description' => 'questionnaire 2 description',
  ]);

  // how to handle the link table checking.

  // check that the questionnaire has been written to the db then grab the new id ready to use as input to the link table.
  // We don't have to search for the questionnaire id as we set that above and so we already know what it should have been set to.
  $questionnaire = $I->grabRecord('questionnaires', ['title' => 'Questionnaire 2']);
  //$I->seeRecord('questionnaires_question', ['questionnaires_id' => $questionnaire->id, 'question_id' => '9900']);

  // Then
  $I->seeCurrentUrlEquals('/admin/questionnaires');
  $I->see('Questionnaires');
  $I->see('Questionnaire 1');
  $I->see('Questionnaire 2');

  $I->click('Questionnaire 2'); // the title is a link to the description page


  // Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
  $I->seeCurrentUrlMatches('~/admin/questionnaires/(\d+)~');
  $I->see('questionnaire 2');
  $I->see('questionnaire 2 description');
//  $I->see('creator: testuser1');
//  $I->see('questionnaires:');
//  $I->see('questionnaire 1');



  // create an questionnaire linked to two questionnaires
  // When
  $I->amOnPage('/admin/questionnaires');
  $I->see('questionnaires');
  $I->see('questionnaire 1');
  $I->dontSee('questionnaire 3');
  // And
  $I->click('Add Questionnaire');

  // Then
  $I->amOnPage('/admin/questionnaires/create');
  // And
  $I->see('Add questionnaire');
  $I->submitForm('#createquestionnaire', [
      'title' => 'questionnaire 3',
      'description' => 'questionnaire 3 content',
      'questionnaire' => '[questionnaire1, questionnaire2]', // multi select drop down
  ]);

  // how to handle the link table checking.

          /* On your controller

  $questionnaires = questionnaire::lists('name', 'id');

          On customer create view you can use.

          {{ Form::label('questionnaires', 'questionnaires') }}
          {{ Form::select('questionnaires[]', $questionnaires, null, ['id' => 'questionnaires', 'multiple' => 'multiple']) }}

          Third parameter accepts a list of array as well. If you define a relationship on your model you can do this:

          {!! Form::label('questionnaires', 'questionnaires') !!}
          {!! Form::select('questionnaires[]', $questionnaires, $questionnaire->questionnaires->lists('id')->all(), ['id' => 'questionnaires', 'multiple' => 'multiple']) !!}

          */

  // check that the questionnaire has been written to the db then grab the new id ready to use as input to the link table.
  // We don't have to search for the questionnaire id as we set that above and so we already know what it should have been set to.
  $questionnaire = $I->grabRecord('questionnaires', ['title' => 'questionnaire 2']);
//  $I->seeRecord('questionnaire_questionnaire', ['questionnaire_id' => $questionnaire->id, 'questionnaire_id' => '9900']);
//  $I->seeRecord('questionnaire_questionnaire', ['questionnaire_id' => $questionnaire->id, 'questionnaire_id' => '9901']);

  // Then
  $I->seeCurrentUrlEquals('/admin/questionnaires');
  $I->see('Questionnaires');
  $I->see('questionnaire 1');
  $I->see('questionnaire 2');
  $I->see('questionnaire 3');

  $I->click('questionnaire 3'); // the title is a link to the description page


  // Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
  //$I->seeCurrentUrlMatches('~/admin/questionnaires/(\d+)~');
  $I->see('questionnaire 3');
  $I->see('questionnaire 3 content');
  $I->see('creator: testuser1');
  $I->see('questionnaires:');
  $I->see('questionnaire 1');
  $I->see('questionnaire 2');
