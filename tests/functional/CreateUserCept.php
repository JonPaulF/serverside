<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new user');

//when
$I->amOnPage('/admin/users');
$I->see('Users', 'h1');
$I->dontSee('Window');
//and
$I->click('Add User');
//Then
$I->amOnPage('/admin/users/create');
//and
$I->see('Add user', 'h1');
$I->submitForm('.createuser',[
  'name' => 'Jon Paul',
  'email' => '23178132@edgehill.ac.uk',
  'password' => 'password'
]);
//then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Users', 'h1');
$I->see('New user added!');
$I->see('Jon Paul');


//check for duplicates

//when
$I->amONPage('/admin/users');
$I->see('Users', 'h1');
$I->see('Jon Paul');

//and
$I->click('Add User');

//then
$I->amOnPage('/admin/users/create');

$I->see('Add User', 'h1');
$I->submitForm('.createuser',[
  'name' => 'Jon Paul',
  'email' => '23178132@edgehill.ac.uk',
  'password' => 'password'
]);
//then
$I->seeCurrentUrlEquals('/admin/users');
$I->see('Users', 'h1');
$I->see('Error user already exists!');
