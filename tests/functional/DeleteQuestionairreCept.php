<?php
  $I = new FunctionalTester($scenario);

  $I->am('user');
  $I->wantTo('delete an questionairre');


  // Add db test data

  // add a test user
  $I->haveRecord('users', [
      'id' => '9999',
      'name' => 'testuser1',
      'email' => 'test1@user.com',
      'password' => 'password',
  ]);

  // Add test category
  $I->haveRecord('categories', [
      'id' => '9900',
      'title' => 'category1',
      'detail' => 'category1 detail',
  ]);



  // add a test questionairre to delete
  $I->haveRecord('questionairres', [
      'id' => '9000',
      'title' => 'questionairre 1',
      'content' => 'questionairre 1 content',
      'slug' => 'questionairre1',
      'user_id' => 'testuser1',
  ]);


  // add link data for questionairre and category for the test entry
  $I->haveRecord('questionairre_category', [
      'questionairre_id' => '9090',
      'category_id' => '9900',
  ]);


  // Check the user is in the db and can be seen
  $I->seeRecord('questionairres', ['title' => 'questionairre1', 'id' => '9000']);


  // When
  $I->amOnPage('/admin/questionairres');

  // then

  // Check  the link is present - this is because there could potentially be many update links/buttons.
  // each link can be identified by the users id as name.
  $I->seeElement('a', ['name' => '9000']);
  // And
  $I->click('Delete questionairre1');

  // Then
  $I->amOnPage('/admin/questionairres');
  // And
  $I->dontSeeElement('a', ['name' => '9000']);
